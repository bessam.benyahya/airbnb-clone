import { format } from "date-fns";
import { useRouter } from "next/router";
import Footer from "../components/Footer";
import Header from "../components/Header";
import InfoCard from "../components/InfoCard";
import Map from "../components/Map";

function Search({ searchResults }) {
  const router = useRouter();

  console.log(searchResults);

  const { location, startDate, endDate, numberOfGuests } = router.query;

  const formattedStartDate = format(new Date(startDate), "dd MMMM yy");
  const formattedEndDate = format(new Date(endDate), "dd MMMM yy");
  const range = `${formattedStartDate} - ${formattedEndDate}`;

  const placeholder = `${location} | ${range} | ${numberOfGuests} ${
    numberOfGuests > 1 ? "guests" : "guest"
  }`;

  return (
    <div>
      <Header placeholder={placeholder} />
      <main className="grid grid-cols-1 lg:grid-cols-3">
        <section className="flex-grow pt-14 px-6 lg:col-span-2">
          <p className="text-xs">
            300+ - {range} - Stays for {numberOfGuests}{" "}
            {numberOfGuests > 1 ? "guests" : "guest"}
          </p>
          <h1 className="text-3xl font-semibold mt-2 mb-6">
            Stay in {location}
          </h1>

          <div className="hidden md:inline-flex mb-5 space-x-3 text-gray-800 whitespace-nowrap">
            <p className="custom-p-button">Cancellation Flexibility</p>
            <p className="custom-p-button">Type of Place</p>
            <p className="custom-p-button">Price</p>
            <p className="custom-p-button">Rooms and Beds</p>
            <p className="custom-p-button">More filters</p>
          </div>

          <div className="flex flex-col">
            {searchResults.map((item, id) => (
              <InfoCard key={id} {...item} />
            ))}
          </div>
        </section>

        <section className="hidden lg:inline-flex lg:col-span-1 lg:flex-grow">
          <Map searchResults={searchResults} />
        </section>
      </main>
      <Footer />
    </div>
  );
}

export default Search;

export async function getServerSideProps() {
  const searchResults = await fetch("https://jsonkeeper.com/b/5NPS").then(
    (res) => res.json()
  );

  return {
    props: {
      searchResults,
    },
  };
}
