import Image from "next/image";
import HomeBannerImage from "/public/home-banner--djerba.jpeg";

function Banner() {
  return (
    <div className="relative h-[300px] sm:h-[400px] lg:h-[500px] xl:h-[600px] 2-xl:h-[700px]">
      <Image src={HomeBannerImage} layout="fill" objectFit="cover" />
      <div className="absolute top-1/2 w-full text-center">
        <p className="text-sm sm:text-lg text-black font-semibold">
          NOT SURE WHERE TO GO? PERFECT.
        </p>

        <button className="text-white bg-black px-10 py-4 shadow-md rounded-full font-bold my-3 hover:shadow-xl active:scale-90 transition duration-150">
          I'm flexible
        </button>
      </div>
    </div>
  );
}

export default Banner;
