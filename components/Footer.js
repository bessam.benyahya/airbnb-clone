function Footer() {
  return (
    <footer className="bg-gray-100 text-gray-600">
      <div className="grid grid-cols-1 md:grid-cols-4 gap-y-10 px-32 py-14 max-w-7xl mx-auto">
        <div className="space-y-4 text-xs">
          <h5 className="font-bold text-gray-800">ABOUT</h5>
          <p>How Airbnb works</p>
          <p>Newsroom</p>
          <p>Investors</p>
          <p>Airbnb Plus</p>
          <p>Airbnb Luxe</p>
        </div>
        <div className="space-y-4 text-xs">
          <h5 className="font-bold text-gray-800">COMMUNITY</h5>
          <p>Papa React</p>
          <p>Presents</p>
          <p>Zero to Full Stack Hero</p>
          <p>Hundreds of Students</p>
          <p>Join now</p>
        </div>
        <div className="space-y-4 text-xs">
          <h5 className="font-bold text-gray-800">HOST</h5>
          <p>Papa React</p>
          <p>Presents</p>
          <p>Zero to Full Stack Hero</p>
          <p>Hundreds of Students</p>
          <p>Join now</p>
        </div>
        <div className="space-y-4 text-xs">
          <h5 className="font-bold text-gray-800">SUPPORT</h5>
          <p>Help Centre</p>
          <p>Trust & Safety</p>
          <p>Say Hi Youtube</p>
          <p>Easter Eggs</p>
          <p>For the Win</p>
        </div>
      </div>
    </footer>
  );
}

export default Footer;
