import { useState } from "react";
import ReactMapGL, { Marker, Popup } from "react-map-gl";
import getCenter from "geolib/es/getCenter";

function Map({ searchResults }) {
  const [viewport, setViewport] = useState({
    width: "100%",
    height: "100vh",
    zoom: 8.5,
  });

  const [selectedLocation, setSelectedLocation] = useState({});

  const coordinates = searchResults.map((item) => ({
    latitude: item.lat,
    longitude: item.long,
  }));

  const center = getCenter(coordinates);

  return (
    <ReactMapGL
      {...viewport}
      mapStyle="mapbox://styles/binov/cky2ua3gb13as14ky31vu628d"
      mapboxApiAccessToken={process.env.mapbox_key}
      onViewportChange={(nextViewport) =>
        setViewport({ ...nextViewport, width: "100%", height: "100vh" })
      }
      latitude={center.latitude}
      longitude={center.longitude}
    >
      {searchResults.map((item) => (
        <div key={item.long}>
          <Marker
            longitude={item.long}
            latitude={item.lat}
            offsetLeft={-20}
            offsetTop={-10}
          >
            <p
              role="img"
              className="cursor-pointer text-2xl animate-bounce"
              onClick={() => setSelectedLocation(item)}
              aria-label="push-pin"
            >
              📌
            </p>
          </Marker>

          {/* this is the popup if I click on a Marker */}
          {selectedLocation.long === item.long ? (
            <Popup
              onClose={() => setSelectedLocation({})}
              closeOnClick={true}
              latitude={item.lat}
              longitude={item.long}
            >
              {item.title}
            </Popup>
          ) : (
            false
          )}
        </div>
      ))}
    </ReactMapGL>
  );
}

export default Map;
